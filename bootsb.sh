#!/bin/bash

# ln -s {$path-to-this-file} /usr/local/bin/bootsb

# Text Colors
txtR=$(tput setaf 1)
txtG=$(tput setaf 2)
txtY=$(tput setaf 3)
txtB=$(tput setaf 4)
txtM=$(tput setaf 5)
txtC=$(tput setaf 6)
txtW=$(tput setaf 7)
txtRst=$(tput sgr0)

script=$(readlink -n $0)
bootDir=`dirname $script`

command=start
validCommands="start|stop|restart|update"

case "$#" in
    0)
        echo -e ${txtR} "You must provide a valid sandbox first" ${txtRst} "\n"
        exit 1
        ;;

    1)
        if [[ $1 =~ ^$validCommands$ ]]; then
            echo -e ${txtR} "Commands must be entered after a valid vagrant box" ${txtRst} "\n"
            exit 1
        else
            box=$1
        fi
        ;;

    2)
        if [[ $2 =~ ^$validCommands$ ]]; then
            box=$1
            command=$2
        else
            echo -e ${txtR} "Invalid command - " ${validCommands//[|]/,} ${txtRst} "\n"
            exit 1
        fi
        ;;
esac

cd $bootDir

case "$command" in
    'start')
        vagrant up $box
        vagrant ssh $box
    ;;

    'stop')
        vagrant halt $box
    ;;

    'restart')
        vagrant reload $box
        vagrant ssh $box
    ;;

    'update')
        vagrant up $box
        vagrant provision $box
    ;;

    'rebuild')
        vagrant ssh $box -c "mysqldump -c --opt --all-databases > ~/workspace/mysql_rebuild_bck.sql"
        vagrant halt $box
        vagrant destroy $box
        vagrant up $box
        vagrant ssh $box -c "mysql < ~/workspace/mysql_rebuild_bck.sql"
        vagrant ssh $box
    ;;
esac
