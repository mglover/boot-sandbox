# Boot Sandbox #

Boot Sandbox allows you to run multiple vagrant boxes from a single command from any directory on Linux or Mac CLI.

## Install ##

Simply clone this repo and create a symlink in your local bin directory to the bootsb.sh file.

```
sudo ln -s /{boot-sandbox-dir}/bootsb.sh /usr/local/bin/{boot-command}
```

Where `{boot-sandbox-dir}` is the path to the Boot Sandbox directory and `{boot-command}` can be as a command of your choice, recommended use `bootsb` as it has few if any clashing services.

## Running ##

Boot Sandbox accepts two parameters, `{box-name}` and `{command}`.

`{box-name}` is always required but `{command} by default will run start command (see below for list of commands)

```
bootsb {box-name} {command}
```

## Boxes ##

#### Apache Box ####

This contains Apache2, PHP5, MySQL(MariaDB), xdebug, composer, phing, git, sass

#### Nginx Box ####

This contains Nginx with FPM, PHP5, MySQL(MariaDB), xdebug, composer, phing, git, sass

## Defaults ##

These can be edited in the `Vagrantfile`

#### Synced Folder ####


The local synced folder is set to `/workspace` and the vagrant box synced folder is set to `/var/www/` 

#### IP ####

The Apache box is set to `192.168.56.150`

The Nginx box is set to `192.168.56.160`

## Commands ##

#### start ####

```
bootsb apache
```
or
```
bootsb apache start
```

This performs `vagrant up` followed by `vagrant ssh`

#### stop ####

```
bootsb apache stop
```

This performs `vagrant halt`

#### restart ####

```
bootsb apache restart
```

This performs `vagrant reload` followed by `vagrant ssh`

#### update ####

```
bootsb apache update
```

This performs `vagrant up` followed by `vagrant provision`
