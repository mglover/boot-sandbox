# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_API_VERSION = "2"
VAGRANT_OS = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"
VAGRANT_SYNC_FOLDER = "/workspace"
VAGRANT_HOSTNAME = "sandbox"
VAGRANT_MEMORY = "1024"

APACHE_IP = "192.168.56.150"
NGINX_IP = "192.168.56.160"

Vagrant.configure(VAGRANT_API_VERSION) do |config|
    config.vm.box = "base"
    config.vm.box_url = VAGRANT_OS

    config.vm.synced_folder VAGRANT_SYNC_FOLDER, "/var/www/"

    config.vm.hostname = VAGRANT_HOSTNAME

    config.vm.provider :virtualbox do |vb|
        vb.name = 'BaseSandbox'
        vb.gui = false
        vb.customize ["modifyvm", :id, "--memory", VAGRANT_MEMORY]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    config.vm.define 'apache' do |apache|
        apache.vm.box = "apache"
        apache.vm.network :private_network, ip: APACHE_IP

        apache.vm.provider :virtualbox do |vb|
            vb.name = "ApacheSandbox"
        end

        apache.vm.provision "ansible" do |ansible|
            ansible.verbose = 'v'
            ansible.playbook = "provisioning/playbook-apache.yml"
            ansible.inventory_path = 'provisioning/apache'
            ansible.host_key_checking = false
            ansible.limit = 'apache'
        end
    end

    config.vm.define 'nginx' do |nginx|
        nginx.vm.box = "nginx"
        nginx.vm.network :private_network, ip: NGINX_IP

        nginx.vm.provider :virtualbox do |vb|
            vb.name = "NginxSandbox"
        end

        nginx.vm.provision "ansible" do |ansible|
            ansible.verbose = 'v'
            ansible.playbook = "provisioning/playbook-nginx.yml"
            ansible.inventory_path = 'provisioning/nginx'
            ansible.host_key_checking = false
            ansible.limit = 'nginx'
        end
    end
end